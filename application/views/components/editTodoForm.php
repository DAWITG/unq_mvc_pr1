<h2>Update ToDo Form</h2>
<form action="<?php echo BASEURL;?>/todo/updateTodo" method="POST">
<div class="form-group">
<input type="text" name="subject" class="form-control" placeholder="Task Subject..." value="<?php echo $data['data']->subject; ?>">
<div class="error">
    <?php if($data['subjectError']): echo $data['subjectError']; endif;?>
</div>
</div>
<div class="form-group">
<input type="text" name="detail" class="form-control" placeholder="Detail Description..." value="<?php echo $data['detail']->detail; ?>">
<div class="error">
    <?php if($data['detailError']): echo $data['detailError']; endif;?>
</div>
</div>
<div class="form-group">
<input type="number" name="noofday" class="form-control" placeholder="No of Days..." value="<?php echo $data['noofday']->noofday; ?>">
<div class="error">
    <?php if($data['noofdayError']): echo $data['noofdayError']; endif;?>
</div>
<input type="hidden" name="hiddenId" value="<?php echo $data['data']->id; ?>">
</div>



<div class="form-group">
    <input type="submit" value="Update ToDo" class="btn btn-primary">
</div>

</form>