<table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Subject</th>
                <th>Detail</th>
                <th># of Days</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tbody>
      <?php if(!empty($data)): ?>

      <?php foreach($data as $todo): ?>

      <tr>
          <td><?php echo ucwords($todo->subject); ?></td>
          <td><?php echo ucwords($todo->detail); ?></td>
          <td><?php echo ucwords($todo->noofday); ?></td>
          <td><a href="<?php echo BASEURL; ?>/todo/edit_todo/<?php echo $todo->id; ?>" class="btn btn-warning">Edit</a></td>
          <td><a href="<?php echo BASEURL; ?>/todo/delete/<?php echo $todo->id; ?>" class="btn btn-danger">Delete</a></td>
      </tr>

<?php endforeach;?>

<?php endif; ?> 
</tbody>
           
    </table>