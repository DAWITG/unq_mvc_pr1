<h2>Create new user</h2>
    <form action="<?php echo BASEURL; ?>/userController/createUser" method="POST">

    <div class="form-group">
    <input type="text" name="firstName" class="form-control" placeholder="First Name..." value="<?php if(!empty($data['firstName'])): echo $data['firstName']; endif; ?>">
    <div class="error">
        <?php if(!empty($data['firstNameError'])): echo $data['firstNameError']; endif; ?>
    </div>
    </div>
    <!-- Close form-group -->
	 <div class="form-group">
    <input type="text" name="lastName" class="form-control" placeholder="Last Name..." value="<?php if(!empty($data['lastName'])): echo $data['lastName']; endif; ?>">
    <div class="error">
        <?php if(!empty($data['lastNameError'])): echo $data['lastNameError']; endif; ?>
    </div>
    </div>
    <!-- Close form-group -->
    <div class="form-group">
    <input type="email" name="email" class="form-control" placeholder="Email..." value="<?php if(!empty($data['email'])): echo $data['email']; endif; ?>">
    <div class="error">
        <?php if(!empty($data['emailError'])): echo $data['emailError']; endif; ?>
    </div>
    </div>
    <!-- Close form-group -->
    <div class="form-group">
    <input type="password" name="password" class="form-control" placeholder="Create new password..." value="<?php if(!empty($data['password'])): echo $data['password']; endif; ?>">
    <div class="error">
        <?php if(!empty($data['passwordError'])): echo $data['passwordError']; endif; ?>
    </div>
    </div>
    <!-- Close form-group -->
    <div class="form-group">
    <input type="submit" name="singupBtn" class="btn btn-primary" value="Register">
    </div>
    <!-- Close form-group -->

    </form>