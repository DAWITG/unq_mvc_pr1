<h2>Add Todo Form</h2>
<form action="<?php echo BASEURL;?>/todo/todoList" method="POST">
<div class="form-group">
<input type="text" name="subject" class="form-control" placeholder="Task Subject..." value="<?php if($data['subject']): echo $data['subject']; endif; ?>">
<div class="error">
    <?php if($data['subjectError']): echo $data['subjectError']; endif;?>
</div>
</div>
<div class="form-group">
<input type="text" name="detail" class="form-control" placeholder="Detail Description..." value="<?php if($data['detail']): echo $data['detail']; endif; ?>">
<div class="error">
    <?php if($data['detailError']): echo $data['detailError']; endif;?>
</div>
</div>
<div class="form-group">
<input type="number" name="noofday" class="form-control" placeholder="Planed # Days..." value="<?php if($data['noofday']): echo $data['noofday']; endif; ?>">
<div class="error">
    <?php if($data['noofdayError']): echo $data['noofdayError']; endif;?>
</div>
</div>

<div class="form-group">
    <input type="submit" value="Add ToDo" class="btn btn-primary">
</div>

</form>