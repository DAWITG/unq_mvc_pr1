<?php 

class todoModel extends database {

    public function addTodo($todo){

        if($this->Query("INSERT INTO todo(subject, detail, noofday, userId) VALUES (?,?,?,?)", $todo)){
            return true;
        }

    }

    public function getData($userId){

        if($this->Query("SELECT * FROM todo WHERE userId = ? ", [$userId])){

            $data = $this->fetchAll();
            return $data;

        }

    }

    public function edit_data($id, $userId){

        if($this->Query("SELECT * FROM todo WHERE id = ? && userId = ? ", [$id, $userId])){

            $row = $this->fetch();
            return $row;

        }

    }

    public function updateTodo($updateData){

        if($this->Query("UPDATE todo SET subject = ? , detail = ? , noofday = ? WHERE id = ? AND userId = ? ", $updateData)){

            return true;

        }

    }

    public function deleteTodo($id, $userId){

        if($this->Query("DELETE FROM todo WHERE id = ? && userId = ? ", [$id, $userId])){
            return true;
        }

    }

}


?>