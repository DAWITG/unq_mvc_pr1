<?php

class userController extends framework {


    public function __construct(){

        if($this->getSession('userId')){
            $this->redirect("todo");
        }
        $this->helper("link");
        $this->userModel = $this->model('userModel');
        
    }

    public function index(){

        $this->view("signup");
    }

    public function createUser(){

        $userData = [

         'firstName'        => $this->input('firstName'),
		 'lastName'        => $this->input('lastName'),
         'email'           => $this->input('email'),
         'password'        => $this->input('password'),
         'firstNameError'   => '',
		 'lastNameError'   => '',
         'emailError'      => '',
         'passwordError'   => '' 

        ];

        if(empty($userData['firstName'])){

            $userData['firstNameError'] = 'First Name is required';

        }
		if(empty($userData['lastName'])){

            $userData['lastNameError'] = 'Last Name is required';

        }
        if(empty($userData['email'])){
            $userData['emailError'] = 'Email is required';
        } else {
            if(!$this->userModel->checkEmail($userData['email'])){

             $userData['emailError'] = "Sorry this email is already exist";

            }
        }

        if(empty($userData['password'])){
            $userData['passwordError'] = "Password is required";
        } else if(strlen($userData['password']) < 5 ){
            $userData['passwordError'] = "Passowrd must be 5 characters long";
        }

        if(empty($userData['firstNameError']) && empty($userData['lastNameError']) && empty($userData['emailError']) && empty($userData['passwordError'])){
            
            $password = password_hash($userData['password'], PASSWORD_DEFAULT);
            $data = [$userData['firstName'],$userData['lastName'], $userData['email'], $password];
            if($this->userModel->createUser($data)){
                
                $this->setFlash("userCreated", "Your user account has been created successfully");
                $this->redirect("userController/loginForm");

            }

        } else {
            $this->view('signup', $userData);
        }

    }

    public function loginForm(){
        $this->view("login");
    }

    public function userLogin(){

        $userData = [

         'email'         => $this->input('email'),
         'password'      => $this->input('password'),
         'emailError'    => '',
         'passwordError' => ''

        ];

        if(empty($userData['email'])){
            $userData['emailError'] = "Email is required";
        }

        if(empty($userData['password'])){
            $userData['passwordError'] = "Password is required";
        }

        if(empty($userData['emailError']) && empty($userData['passwordError'])){

            $result = $this->userModel->userLogin($userData['email'], $userData['password']);
            if($result['status'] === 'emailNotFound'){
                $userData['emailError'] = "Sorry invalid email";
                $this->view("login", $userData);
            } else if($result['status'] === 'passwordNotMacthed'){
                $userData['passwordError'] = "Sorry invalid password";
                $this->view("login", $userData);
            } else if($result['status'] === "ok"){
                $this->setSession("userId", $result['data']);
                $this->redirect("profile");
            }
;
        } else {
            $this->view("login", $userData);
        }

    }

}


?>