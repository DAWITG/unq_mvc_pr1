<?php 

class todo extends framework {

    public function __construct()
    {
      if(!$this->getSession('userId')){

        $this->redirect("userController/loginForm");

      }
       $this->helper("link");
       $this->todoModel = $this->model("todoModel"); 
    }
    public function index(){
     $userId = $this->getSession('userId');
      $data = $this->todoModel->getData($userId);
 
      $this->view("todo", $data);

    }

    public function todoForm(){
      $this->view("addToDo");
    }

    public function todoList(){
      
      $todoData = [

       'subject'           => $this->input('subject'),
       'detail'          => $this->input('detail'),
       'noofday'        => $this->input('noofday'),
       'subjectError'      => '',
       'detailError'     => '',
       'noofdayError'   => ''

      ];

      if(empty($todoData['subject'])){
        $todoData['subjectError'] = "Subject is required";
      }
      if(empty($todoData['detail'])){
        $todoData['detailError'] = "Detail is required";
      }
      if(empty($todoData['noofday'])){
        $todoData['noofdayError'] = "Number of days is required";
      }

      if(empty($todoData['subjectError']) && empty($todoData['detailError']) && empty($todoData['noofdayError'])){

        $data = [$todoData['subject'], $todoData['detail'], $todoData['noofday'], $this->getSESSION('userId')];
         if($this->todoModel->addToDo($data)){
                $this->setFlash("todoAdded", "Your task has been added successfuly");
                $this->redirect("todo/index");
         }


      } else {
        $this->view("addToDo", $todoData);
      }

    }

    public function edit_todo($id){
      
      $userId = $this->getSession('userId');
      $todoEdit = $this->todoModel->edit_data($id, $userId);
      $data = [

        'data'    => $todoEdit,
        'subjectError' => '',
        'detailError' => '',
        'noofdayError' => ''

      ];
      $this->view("edit_todo", $data);

    }

    public function updateTodo(){

      $id = $this->input('hiddenId');
      $userId = $this->getSession('userId');
      $todoEdit = $this->todoModel->edit_data($id, $userId);
      $todoData = [

        'subject'           => $this->input('subject'),
        'detail'          => $this->input('detail'),
        'noofday'        => $this->input('noofday'),
        'data'           => $todoEdit,
        'hiddenId'       => $this->input('hiddenId'),
        'subjectError'      => '',
        'detailError'     => '',
        'noofdayError'   => ''
        
 
       ];
 
       if(empty($todoData['subject'])){
         $todoData['subjectError'] = "Task subject is required";
       }
       if(empty($todoData['detail'])){
         $todoData['detailError'] = "Task detail is required";
       }
       if(empty($todoData['noofday'])){
         $todoData['noofdayError'] = "Planned number of days is required";
       }

       if(empty($todoData['subjectError']) && empty($todoData['detailError']) && empty($todoData['noofdayError'])){
       
        $updateData = [$todoData['subject'], $todoData['detail'], $todoData['noofday'], $todoData['hiddenId'], $userId];

        if($this->todoModel->updateTodo($updateData)){

          $this->setFlash('todoUpdated', 'Your todo record has been updated successfully');
          $this->redirect("todo/index");

        }

       } else {
        $this->view("edit_todo", $todoData);
       }

    }

    public function delete($id){

      $userId = $this->getSession('userId');
      if($this->todoModel->deleteTodo($id, $userId)){
        $this->setFlash('deleted', 'Your todo has been deleted successfully');
        $this->redirect('todo/index');
      }

    }



    public function logout(){

        $this->destroy();
        $this->redirect("userController/loginForm");

    }

}


?>